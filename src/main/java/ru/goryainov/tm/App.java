package ru.goryainov.tm;

import ru.goryainov.tm.dao.ProjectDAO;
import ru.goryainov.tm.dao.TaskDAO;

import java.util.Scanner;

import static ru.goryainov.tm.constant.TerminalConst.*;

/**
 * Тестовое приложение с поддержкой аргументов запуска
 */

public class App {

    private static final ProjectDAO projectDAO = new ProjectDAO();

    private static final TaskDAO taskDAO = new TaskDAO();

    private static final Scanner scanner = new Scanner(System.in);

    /**
     * Точка входа
     *
     * @param args параметры запуска
     */
    public static void main(final String[] args) {
        run(args);
        displayWelcome();
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            run(command);
        }
    }

    /**
     * Запуск приложения
     *
     * @param args массив параметров запуска
     */
    private static void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    /**
     * Запуск приложения
     *
     * @param param параметр запуска
     */
    private static int run(final String param) {
        if (param == null) return -1;
        switch (param) {
            case VERSION:
                return displayVersion();
            case ABOUT:
                return displayAbout();
            case HELP:
                return displayHelp();
            case EXIT:
                return displayExit();
            case PROJECT_CREATE:
                return createProject();
            case PROJECT_CLEAR:
                return clearProject();
            case PROJECT_LIST:
                return listProject();
            case TASK_CREATE:
                return createTask();
            case TASK_CLEAR:
                return clearTask();
            case TASK_LIST:
                return listTask();
            default:
                return displayError();
        }
    }

    /**
     * Вывод сведений о разработчике
     */
    private static int displayAbout() {
        System.out.println("Andrey Goryainov");
        System.out.println("goryainov_aa@nlmk.ru");
        return 0;
    }

    /**
     * Вывод версии
     */
    private static int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    /**
     * Вывод списка возможных параметров запуска
     */
    private static int displayHelp() {
        System.out.println("version - Display application version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of commands.");
        System.out.println("exit - Terminate console application.");
        System.out.println();
        System.out.println("project-list - display list of projects.");
        System.out.println("project-create - create new project by name.");
        System.out.println("project-clear - remove all projects.");
        System.out.println();
        System.out.println("task-list - display list of tasks.");
        System.out.println("task-create - create new task by name.");
        System.out.println("task-clear - remove all tasks.");
        return 0;
    }

    /**
     * Вывод приветствия
     */
    private static void displayWelcome() {
        System.out.println("-= WELCOME TO TASK MANAGER =-");
    }

    /**
     * Вывод ошибки при вводе параметра запуска не из списка
     */
    private static int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

    /**
     * Вывод завершения приложения
     */
    private static int displayExit() {
        System.out.println("Terminate program...");
        return 0;
    }

    /**
     * Вывод создания проекта
     */
    private static int createProject() {
        System.out.println("[Create project]");
        System.out.println("[Please, enter project name:]");
        final String name = scanner.nextLine();
        projectDAO.create(name);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Вывод очистки проекта
     */
    private static int clearProject() {
        System.out.println("[Clear project]");
        projectDAO.clear();
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Вывод списка проектов
     */
    private static int listProject() {
        System.out.println("[List project]");
        System.out.println(projectDAO.findAll());
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Вывод создания задачи
     */
    private static int createTask() {
        System.out.println("[Create task]");
        System.out.println("[Please, enter task name:]");
        final String name = scanner.nextLine();
        taskDAO.create(name);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Вывод очистки задачи
     */
    private static int clearTask() {
        System.out.println("[Clear task]");
        taskDAO.clear();
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Вывод списка задач
     */
    private static int listTask() {
        System.out.println("[List task]");
        System.out.println(taskDAO.findAll());
        System.out.println("[Ok]");
        return 0;
    }
}
